package com.venko.prueba.ConfigSecurity;

import lombok.Data;

@Data
public class AuthCredentials {
	private String cc;
	private String password;
}
