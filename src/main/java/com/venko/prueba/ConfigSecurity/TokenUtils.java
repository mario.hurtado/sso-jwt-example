
package com.venko.prueba.ConfigSecurity;

import java.security.Key;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class TokenUtils {
    private final static Long ACCESS_TOKEN_VALIDITY_SECONDS = 2_592_000L;
    private final static Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    
    public static String createToken(String nombre, String cc) {
        long expirationTime = ACCESS_TOKEN_VALIDITY_SECONDS * 1_000;
        Date expirationDate = new Date(System.currentTimeMillis() + expirationTime);

        Map<String, Object> extra = new HashMap<>();
        extra.put(nombre, "nombre");
        return Jwts.builder()
                .setSubject(cc)
                .setExpiration(expirationDate)
                .addClaims(extra)
                .signWith(SECRET_KEY)
                .compact();
    }

    public static UsernamePasswordAuthenticationToken getAuthentication(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(SECRET_KEY)
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            String cc = claims.getSubject();
            return new UsernamePasswordAuthenticationToken(cc, null, Collections.emptyList());
        } catch (JwtException e) {
            return null;
        }
    }
}
